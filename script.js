let container = document.querySelector(".container");
let btn = document.getElementById("spin");

const values = {
  1: 100,
  2: 20,
  3: 5,
  4: 10,
  5: 200,
  6: 2,
  7: 0,
  8: 2,
  9: 5,
  10: 10,
  11: 5,
  12: 50,
  13: 20,
  14: 10,
  15: 100,
  16: 5,
};

let canSpin = true;

//array containting the result of the spins
let resultArr = Array.from({ length: 10 }, () => null);

//array that contains the position of the element that has to occure 3 times every 10 spins
let randomThreeIndexArr = [];
let randomTwoIndexArr = [];

//keeps track of how many times the wheel has been spun
let currentSpin = 0;

//keeps track of the last spot the wheel has stopped.
let lastSpot = 1;

//keeps track of the total winnings from the wheel spins
let totalWinnings = 0;

//determines where the wheel will stop spinning
let wheelRotation = 0;

//random position that should occur 3 times and 2 times
let threeRewards = Math.ceil(Math.random() * 16);
let twoRewards = Math.ceil(Math.random() * 16);

//creates 3 random times for the selected position to occur
const setThreeRandomRewards = () => {
  randomThreeIndexArr.push(Math.floor(Math.random() * 10));
  randomThreeIndexArr.push(Math.floor(Math.random() * 10));
  randomThreeIndexArr.push(Math.floor(Math.random() * 10));
  randomThreeIndexArr.sort();

  //checks if there are consecative times
  if (
    randomThreeIndexArr[1] - randomThreeIndexArr[0] < 2 ||
    randomThreeIndexArr[2] - randomThreeIndexArr[1] < 2
  ) {
    randomThreeIndexArr = [];
    setThreeRandomRewards();
  }
};

//creates 2 random times for the selected position to occur
const setTwoRandomRewards = () => {
  let firstRandomIndex = Math.floor(Math.random() * 10);
  let secondRandomIndex = Math.floor(Math.random() * 10);

  //checkes to make sure the 2 times position isn't in the same place as 3 times position
  while (randomThreeIndexArr.includes(firstRandomIndex)) {
    firstRandomIndex = Math.floor(Math.random() * 10);
  }
  while (randomThreeIndexArr.includes(secondRandomIndex)) {
    secondRandomIndex = Math.floor(Math.random() * 10);
  }

  randomTwoIndexArr.push(firstRandomIndex);
  randomTwoIndexArr.push(secondRandomIndex);
  randomTwoIndexArr.sort();

  //checks if there are consecative times
  if (randomTwoIndexArr[1] - randomTwoIndexArr[0] < 2) {
    randomTwoIndexArr = [];
    setTwoRandomRewards();
  }
};

//places the 3 times and 2 times postiions in result array
const setPredeterminedResults = () => {
  //checks 3 times and 2 times positions aren't the same
  while (twoRewards === threeRewards) {
    twoRewards = Math.ceil(Math.random() * 16);
  }

  randomThreeIndexArr.forEach((number) => (resultArr[number] = threeRewards));
  randomTwoIndexArr.forEach((number) => (resultArr[number] = twoRewards));
};

setThreeRandomRewards();
setTwoRandomRewards();
setPredeterminedResults();

const resetPredeterminedRewards = () => {
  resultArr = Array.from({ length: 10 }, () => null);
  randomThreeIndexArr = [];
  randomTwoIndexArr = [];
  threeRewards = 7;
  twoRewards = Math.ceil(Math.random() * 16);
  setThreeRandomRewards();
  setTwoRandomRewards();
  setPredeterminedResults();
};

const updateWinnings = () => {
  setTimeout(() => {
    document.getElementById("winnings").textContent = `${totalWinnings} лв`;
  }, 5000);
};

const autoSpin = () => {
  let currentSpot = Math.ceil(Math.random() * 16);

  wheelRotation = wheelRotation + 1800 + 22.5 * (lastSpot - currentSpot);

  container.style.transform = `rotate(${wheelRotation}deg)`;
  lastSpot = currentSpot;

  if (lastSpot === 7) freeSpins();

  totalWinnings += values[lastSpot];
  updateWinnings();
};

const freeSpins = () => {
  canSpin = false;
  setTimeout(() => {
    autoSpin();
    setTimeout(() => {
      autoSpin();
      setTimeout(() => {
        autoSpin();
        setTimeout(() => (canSpin = true), 5500);
      }, 6000);
    }, 6000);
  }, 6000);
};

const spinTheWheel = () => {
  if (!canSpin) {
    return;
  }

  canSpin = false;


  if (resultArr[currentSpin]) {
    wheelRotation =
      wheelRotation + 1800 + 22.5 * (lastSpot - resultArr[currentSpin]);
    container.style.transform = `rotate(${wheelRotation}deg)`;
    lastSpot = resultArr[currentSpin];
  } else {
    let currentSpot = Math.ceil(Math.random() * 16);

    while (currentSpot === threeRewards || currentSpot === twoRewards) {
      currentSpot = Math.ceil(Math.random() * 16);
    }

    wheelRotation = wheelRotation + 1800 + 22.5 * (lastSpot - currentSpot);

    container.style.transform = `rotate(${wheelRotation}deg)`;
    lastSpot = currentSpot;
  }
  totalWinnings += values[lastSpot];
  updateWinnings();
  currentSpin++;

  if (lastSpot === 7) {
    freeSpins();
  }

  //after 10 spins it sets new predetermined rewards for the next 10 spins
  if (currentSpin === 10) {
    resetPredeterminedRewards();
    currentSpin = 0;
  }

  if (lastSpot !== 7) setTimeout(() => (canSpin = true), 5100);
};

btn.onclick = () => {
  spinTheWheel();
};
